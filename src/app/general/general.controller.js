(function() {
  'use strict';

  angular
    .module('project')
    .controller('GeneralController', GeneralController);
  /** @ngInject */
  function GeneralController($scope, Projects, Groups) {
    $scope.getProjects = function () {
      $scope.projects = Projects.query();
    };
    $scope.getGroups = function () {
      $scope.groups = Groups.query();
    };
  }
})();
