'use strict';

angular
  .module('project')
  .factory('Groups', function ($resource, $window) {
    return $resource(
      'https://gitlab.com/api/v3/groups/:id',
      {id: '@id'}
    )
  });