(function() {
  'use strict';

  angular
    .module('project')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      }).state('general', {
        url: '/general',
        templateUrl: 'app/general/general.html',
        controller: 'GeneralController',
        controllerAs: 'general'
      })
      ;

    $urlRouterProvider.otherwise('/home');
  }

})();
