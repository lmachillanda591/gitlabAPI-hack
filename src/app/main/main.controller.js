(function() {
  'use strict';

  angular
    .module('project')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($scope, Login) {
    $scope.access = access;
    $scope.user = {}
    
    function access() {
      Login.access($scope.user, function () {
        console.log("aqui");
      });
    }
  }
})();
