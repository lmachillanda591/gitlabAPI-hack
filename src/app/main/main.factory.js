'use strict';

angular
  .module('project')
  .factory('Login', function ($http, $window, $state) {

    return {
      access: function (user, callback) {
        
        $http({
          method: 'post',
          url: 'https://gitlab.com/api/v3/session',
          data: user
        })
        .success(function (data) {        
          if (callback) callback(data);
          console.log(data);
          $state.go('general');
          $window.sessionStorage.setItem('user', data.private_token);       
        }); 
      },
      isConnected: function () {
        return !!$window.sessionStorage.user;
      }
    }
  });