'use strict';

angular
  .module('project')
  .factory('Projects', function ($resource, $window) {
    return $resource(
      'https://gitlab.com/api/v3/projects/:id',
      {id: '@id'}
    )
  });